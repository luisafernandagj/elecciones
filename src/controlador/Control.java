package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import modeloVO.Candidato;
import modeloVO.Eleccion;
import modeloVO.Estamento;
import modeloVO.TipoDocumento;
import modeloVO.Votante;
import modeloVO.Voto;
import vista.Formulario_1;

public class Control implements ActionListener {

    private Formulario_1 vista;
    private Votante votante;
    private Candidato candidato;
    private Eleccion eleccion;
    private Estamento estamento;
    private TipoDocumento tipodocumento;
    private Voto voto;
    private DefaultTableModel registros;

    public Control(Formulario_1 vista) {
        this.vista = vista;
        ActionListener(this);
    }

    private void ActionListener(ActionListener control) {
        vista.btnLimpiar.addActionListener(control);
        vista.btnVotar1.addActionListener(control);
        vista.btnResultados.addActionListener(control);
    }

    @Override
    public void actionPerformed(ActionEvent evento) {

        if (evento.getActionCommand().contentEquals("LIMPIAR")) {
            vista.txtid.setText("");
            vista.txtnombre.setText("");
            vista.txtemail.setText("");
            vista.txtdocumento.setText("");
            JOptionPane.showMessageDialog(null, "Se Limpió Todo");
            
        } if (evento.getActionCommand().contentEquals("VOTAR")){
            String id = vista.txtid.getText();
            String nombre = vista.txtnombre.getText();
            String email = vista.txtemail.getText();
            String documento = vista.txtdocumento.getText();
            int tipoDocumento = vista.cmbxtipodocumento.getSelectedIndex();
            JOptionPane.showMessageDialog(null, "Registro exitoso de los datos del Votante y del Voto!");
        }
    }   

}
