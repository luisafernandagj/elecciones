
package modeloVO;
import java.sql.Timestamp;
import java.sql.Date;

/**
 *
 * @author 57320
 */
public class Voto {
    Integer id;
    Integer estamento;
    String uuid;
    String enlace;
    Timestamp fechacreacion;
    Timestamp fechavoto;
    Votante votante;
    Candidato candidato;

    public Voto() {
    }

    public Voto(Integer id, Integer estamento, String uuid, String enlace, Timestamp fechacreacion, Timestamp fechavoto, Votante votante, Candidato candidato) {
        this.id = id;
        this.estamento = estamento;
        this.uuid = uuid;
        this.enlace = enlace;
        this.fechacreacion = fechacreacion;
        this.fechavoto = fechavoto;
        this.votante = votante;
        this.candidato = candidato;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getEstamento() {
        return estamento;
    }

    public void setEstamento(Integer estamento) {
        this.estamento = estamento;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getEnlace() {
        return enlace;
    }

    public void setEnlace(String enlace) {
        this.enlace = enlace;
    }

   public Timestamp getFechacreacion() {
        return fechacreacion;
    }

    public void setFechacreacion(Timestamp fechacreacion) {
        this.fechacreacion = fechacreacion;
    }

    public Timestamp getFechavoto() {
        return fechavoto;
    }

    public void setFechavoto(Timestamp fechavoto) {
        this.fechavoto = fechavoto;
    }


    public Votante getVotante() {
        return votante;
    }

    public void setVotante(Votante votante) {
        this.votante = votante;
    }

    public Candidato getCandidato() {
        return candidato;
    }

    public void setCandidato(Candidato candidato) {
        this.candidato = candidato;
    }
}
