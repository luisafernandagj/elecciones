
package modeloDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import modeloVO.Estamento;

    public class EstamentoDAO {
    private Connection connection;

    public EstamentoDAO(Connection connection) {
        this.connection = connection;
    }

    public void insertarEstamento(Estamento estamento) throws SQLException {
        String sql = "INSERT INTO estamento (id, eleccion, descripcion) VALUES (?, ?, ?)";

        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, estamento.getId());
            statement.setInt(2, estamento.getEleccion());
            statement.setString(3, estamento.getDescripcion());

            statement.executeUpdate();
        } catch (SQLException e) {
            throw new SQLException(e);
        }
    }

    public Estamento obtenerEstamentoPorId(int id) throws SQLException {
        String sql = "SELECT * FROM estamento WHERE id = ?";
        Estamento estamento = null;

        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, id);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                estamento = new Estamento();
                estamento.setId(resultSet.getInt("id"));
                estamento.setEleccion(resultSet.getInt("eleccion"));
                estamento.setDescripcion(resultSet.getString("descripcion"));
            }
        } catch (SQLException e) {
            throw new SQLException(e);
        }

        return estamento;
    }

    public void actualizarEstamento(Estamento estamento) throws SQLException {
        String sql = "UPDATE estamento SET eleccion = ?, descripcion = ? WHERE id = ?";

        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, estamento.getEleccion());
            statement.setString(2, estamento.getDescripcion());
            statement.setInt(3, estamento.getId());

            statement.executeUpdate();
        } catch (SQLException e) {
            throw new SQLException(e);
        }
    }
}


